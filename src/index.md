---
marp: true
theme: gricad
author: F. Pérignon, P-A. Bouttier
paginate: true
footer: "14 décembre 2022 - F. Pérignon, P-A Bouttier"
---

# Atelier GitLab
## Open Science Days 2022

*franck.perignon@univ-grenoble-alpes.fr*
*pierre-antoine.bouttier@univ-grenoble-alpes.fr*

14 décembre 2022

---
### Tronc Commun

- [Introduction à Git](https://osd2022-ateliers-gitlab.gricad-pages.univ-grenoble-alpes.fr/supports/git.html)
- [Le langage Markdown](https://osd2022-ateliers-gitlab.gricad-pages.univ-grenoble-alpes.fr/supports/markdown.html)
- [Découverte de GitLab](https://osd2022-ateliers-gitlab.gricad-pages.univ-grenoble-alpes.fr/supports/gitlab.html)

### À la carte

- [Les issues](https://osd2022-ateliers-gitlab.gricad-pages.univ-grenoble-alpes.fr/supports/issues.html)
- [Les merge requests](https://osd2022-ateliers-gitlab.gricad-pages.univ-grenoble-alpes.fr/supports/merge_requests.html)
- [L'intégration continue](https://osd2022-ateliers-gitlab.gricad-pages.univ-grenoble-alpes.fr/supports/cicd.html)
- [Aller plus loin avec Git](https://osd2022-ateliers-gitlab.gricad-pages.univ-grenoble-alpes.fr/supports/git.html)
