---
marp: true
theme: gricad
author: Pierre-Antoine Bouttier
paginate: true
footer: "14 décembre 2022, F. Pérignon & P.-A. Bouttier"
---

# Le langage markdown

Le Markdown est un langage très simple à apprendre, à lire et à écrire qui permet de formater du texte pour une page web. Sur Gitlab vous pouvez utiliser une version étendue du Markdown (gitlab flavored markdown) pour rédiger vos commentaires, issues, fichier d’aide etc.

Documentation
- https://docs.gitlab.com/ee/user/markdown.html
- https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

---
## Le langage Markdown

**L'apprentissage de l'écriture en markdown** est très rapide. Prenez ce temps, vous verrez que c'est à la fois très pratique et très utilisé. 

Les avantages du markdown :
* Basé sur des fichiers textes bruts, donc très interopérables (contrairement aux .doc, .docx, par exemple), et facile à mettre dans un dépôt git/gitlab.
* Des règles très simples
* Facile à convertir : html, pdf, jpg, png, etc...

---
## Le langage markdown

* Au cours de la formation, vous avez pu voir des fichiers dont le nom se finissait par `.md` : c'est l'extension par défaut pour le markdown. 
* Un fichier markdown est un fichier texte brut avec quelques règles syntaxiques.
* Si vous écrivant un document markdown dans votre dépôt gitlab, Gitlab l'affichera formatté. 
* Il est très conseillé, pour chaque projet git/gitlab, d'écrire un fichier README.md qui décrira le projet et ce qu'il y a savoir à son propos.

---
## Le langage markdown

```markdown
# Titre de niveau 1

*Pour écrire en italique*, **pour écrire en gras**, ~~du texte barré~~.

## titre de niveau 2

voici une liste : 
- Élément 2
- Élément 1

### Titre de niveau 3

Une liste ordonnée : 
1. Élément 1
2. Élément 2
```

Les supports de ce stage sont intégralement écrits en markdown. 
